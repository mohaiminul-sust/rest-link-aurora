import React, { Component } from 'react';
import './App.css';
import config from './config.json';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
import { LinkedIn } from 'react-linkedin-login-oauth2';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import QueryString from 'query-string';

let state = { isAuthenticated: false, token: '', code: '', gtoken: ''};

class App extends Component {
  constructor() {
    super();
    this.state = state;
  }

  componentDidMount() {
    //for catching the linked in code from callback query param
    // console.log(window.location.href);
    let params = QueryString.parse(window.location.search);
    if(params.code) {
      console.log(params.code);
      this.setState({isAuthenticated: true, code: params.code})
      this.getCode(params.code);
    } else {
      console.log('No Code Yet!')
    }
  }

  logout = () => {
    this.setState(state);
  };

  linkedInResponse = (response) => {
    console.log(response);
    this.setState({isAuthenticated: true, code: response.code});
  };

  facebookResponse = (response) => {
    console.log(response.accessToken);
    this.setState({isAuthenticated: true, token: response.accessToken});
  };

  googleResponse = (response) => {
    console.log(response.accessToken);
    this.setState({isAuthenticated: true, gtoken: response.accessToken})
  };

  onFailure = (error) => {
    console.log(error);
  };

  getCode = (code) => {
    const axios = require('axios');
    const payload = {
      grant_type: 'authorization_code',
      code: code,
      redirect_uri: encodeURIComponent(config.LINKEDIN_REDIRECT_URI_PROD),
      client_id: config.LINKEDIN_CLIENT_ID,
      client_secret: config.LINKEDIN_CLIENT_SECRET
    }
    const headers= {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    }
    axios.post('https://www.linkedin.com/oauth/v2/accessToken', payload, {headers: headers})
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    const { token, code, gtoken } = this.state;

    let content = !!this.state.isAuthenticated ?
    (
      <div>
          <p>Authenticated</p>
          <div>
              <p>FB Token : { token === '' ? 'Empty' : token }</p>
              <p>LinkedIn Code : { code === '' ? 'Empty' : code }</p>
              <p>Google Token : { gtoken === '' ? 'Empty' : gtoken }</p>
          </div>
          <div>
              <button onClick={this.logout} className="button">
                  Log out
              </button>
          </div>
      </div>
    ) :
    (
      <div>
        <Container className='centered'>
          <Row>
            <Col className='container-cell'>
              <FacebookLogin
                appId={config.FACEBOOK_APP_ID}
                autoLoad={false}
                fields={config.FACEBOOK_SCOPE_FIELDS}
                callback={this.facebookResponse}
              />
            </Col>
            <Col className='container-cell'>
              <GoogleLogin
                clientId={config.GOOGLE_CLIENT_ID}
                buttonText="Login With Google"
                onSuccess={this.googleResponse}
                onFailure={this.onFailure}
                cookiePolicy={'single_host_origin'}
              />
            </Col>
            <Col className='container-cell'>
              <LinkedIn
                clientId={config.LINKEDIN_CLIENT_ID}
                onFailure={this.onFailure}
                onSuccess={this.linkedInResponse}
                redirectUri= {encodeURIComponent(config.LINKEDIN_REDIRECT_URI_PROD)} //"https://rest-link.herokuapp.com/"
                scope={config.LINKEDIN_SCOPE_PARAM}
              ></LinkedIn>
            </Col>
          </Row>
        </Container>
      </div>
    );

    return (
      <div className="App">
          {content}
      </div>
    );
  }
}

export default App;